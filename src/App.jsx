import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Header from './components/Header';
import Map from './components/Map';
import styles from './style.css';

export default class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Map />
      </div>
    );
  }
}

ReactDom.render(<App />, document.getElementById('app'));
