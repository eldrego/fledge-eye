import React from 'react';


const Header = () => {
  return (
    <div className="header">
      <nav className="navbar navbar-default navbar-static-top">
        <div className="container-fluid" />
      </nav>
    </div>
  );
};

export default Header;
