import path from 'path';
import express from 'express';
import open from 'open';
import webpack from 'webpack';
import winston from 'winston';
import configuration from './webpack.config';

import './dotenv';

const app = express();
const baseFile = path.join(__dirname, 'dist/index.html');
const compiler = webpack(configuration);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: configuration.output.publicPath,
}));
app.use(require('webpack-hot-middleware')(compiler));

app.get('/', (req, res) => {
  res.sendFile(baseFile);
});

const port = process.env.PORT || 5000;

app.listen(port, (error) => {
  if (error) {
    winston.log(error);
  } else {
    open(`http://localhost:${port}`);
  }
});
